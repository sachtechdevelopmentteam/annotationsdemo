package sachtech.jainagdev.hdwallpaper.annotationsdemo

import android.app.Activity
import android.view.View
import sachtech.jainagdev.hdwallpaper.annotationsdemo.annotations.BindView
import java.lang.reflect.Field

class ViewBind {

    fun bind(activity:Activity){
bindViews(activity,activity.javaClass.declaredFields,activity.window.decorView)
    }
    fun bindViews(any:Any,fields:Array<Field>,rootView: View){
for (i in 0..fields.size-1) {
    var field = fields[i].getAnnotation(BindView::class.java)
    if (field != null) {
        var id = field.value
        var view = rootView.findViewById<View>(id)
        fields[i].isAccessible = true
        fields[i].set(any, view)
    }
}
    }
}