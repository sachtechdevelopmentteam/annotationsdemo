package sachtech.jainagdev.hdwallpaper.annotationsdemo

import sachtech.jainagdev.hdwallpaper.annotationsdemo.annotations.MyAnnotations

class AnnotatedClass {
    @MyAnnotations(value = ["gjhgjh","uyyu","fgddi","rewer","trtftft"])
    var message:Array<String>?=null

    init {
        var fields=this.javaClass.declaredFields
    for(i in 0..fields.size-1){
        var field=fields[i].getAnnotation(MyAnnotations::class.java)
    fields[i].isAccessible=true
fields[i].set(this@AnnotatedClass,fields[i].getAnnotation(MyAnnotations::class.java).value)
    }
    }
}