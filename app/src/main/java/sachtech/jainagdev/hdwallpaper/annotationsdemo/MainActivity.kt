package sachtech.jainagdev.hdwallpaper.annotationsdemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.TextView
import android.widget.Toast
import sachtech.jainagdev.hdwallpaper.annotationsdemo.annotations.BindView
import java.util.*

class MainActivity : AppCompatActivity() {

    @BindView(R.id.hello_world)
    @JvmField var txtView: TextView? = null
/*@BindView(R.id.hello_world)
var txtView: TextView? = null*/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ViewBind().bind(this)
        txtView!!.text="drfhjdfjhdf"

        var handler= Handler()
        var runnable: Runnable?=null
        runnable=object:Runnable{
            override fun run() {
                updateView()
                handler.postDelayed(runnable,3000)
            }

        }
        handler.postDelayed(runnable,300)

    }

    fun updateView(){
        var ac=AnnotatedClass()

        var rand = Random();
        var randomIndex = rand.nextInt(ac.message?.size?:0);
        var randomElement = ac.message?.get(randomIndex)
        Toast.makeText(this,randomElement,Toast.LENGTH_LONG).show()

    }
}
